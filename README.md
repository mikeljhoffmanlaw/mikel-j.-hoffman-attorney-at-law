At Mikel J. Hoffman, Attorney at Law, we provide quality legal representation to our clients with careful attention to individual needs. Mr. Hoffman gives his personal attention to each client, calling on his more than 30 years of legal experience in a variety of areas to solve problems & plan ahead.

Address: 193 E Main St, Babylon, NY 11702, USA

Phone: 631-661-2121

Website: http://www.nyslaw.com
